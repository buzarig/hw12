"use strict";

const buttons = document.querySelector(".btn-wrapper").children;
const buttonsArray = [...buttons];

buttonsArray.forEach((element) => {
  if (element.classList !== "btn") {
    element.classList.add("btn");
  }
});

function getBlack() {
  buttonsArray.forEach((element) => {
    if (element.classList.contains("active")) {
      element.classList.remove("active");
    }
  });
}

buttonsArray.forEach((button) => {
  document.addEventListener("keydown", (event) => {
    if (
      event.code === "Key" + button.innerHTML ||
      event.code === button.innerHTML
    ) {
      getBlack();

      button.classList.add("active");
    } else if (button.classList.contains("active")) {
      getBlack();
    }
  });
});
